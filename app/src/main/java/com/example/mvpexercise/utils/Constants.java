package com.example.mvpexercise.utils;

public class Constants {

    public static final String INTENT_EXTRA_POST_ID = "POST_ID_EXTRA";
    //Service
    public static final String POSTS_API = "https://jsonplaceholder.typicode.com/";
    public static final String POSTS_ENDPOINT = "posts";
    public static final String GET_COMMENTS_ENDPOINT = "comments";
}
