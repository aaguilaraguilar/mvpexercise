package com.example.mvpexercise.view;

import com.example.mvpexercise.api.model.Comment;

import java.util.ArrayList;

public interface CommentsView extends BaseView {
    void loadComments(ArrayList<Comment> comments);
    void hideLoading();
    void showLoading();
    void showError(String error);
}
