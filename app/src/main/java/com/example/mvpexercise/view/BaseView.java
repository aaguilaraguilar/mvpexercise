package com.example.mvpexercise.view;

import android.content.Context;

public interface BaseView {
    Context getContext();
}
