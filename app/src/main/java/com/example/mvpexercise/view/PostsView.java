package com.example.mvpexercise.view;

import com.example.mvpexercise.api.model.Post;

import java.util.ArrayList;

public interface PostsView extends BaseView {
    void loadPosts(ArrayList<Post> posts);
    void hideLoading();
    void showLoading();
    void showError(String error);
}
