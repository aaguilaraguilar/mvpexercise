package com.example.mvpexercise.business;

import android.content.Context;

import com.example.mvpexercise.BuildConfig;
import com.example.mvpexercise.api.client.PostsRestService;
import com.example.mvpexercise.api.model.GetPostsResponseCallback;
import com.example.mvpexercise.api.model.Post;
import com.example.mvpexercise.application.Application;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import retrofit2.HttpException;

public class PostsInteractor {
    private Application application;
    private PostsRestService postsRestService;
    private Context context;
    private Disposable disposable = Disposables.empty();

    public PostsInteractor(Context context) {
        this.application = Application.getInstance(context);
        this.postsRestService = application.getPostsRestService();
        this.context = context;
    }
    public void getPosts(GetPostsResponseCallback callback){
        disposable = this.postsRestService.getPosts().subscribeOn(application.SubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(posts -> onSuccess(posts, callback),
                            throwable -> onError(throwable, callback));
    }

    @SuppressWarnings("unchecked")
    private void onSuccess(ArrayList<Post> postsResponse, GetPostsResponseCallback getPostsResponseCallback){
        if(postsResponse != null && postsResponse.size() > 0){
            getPostsResponseCallback.responseWSOk(postsResponse);
        } else{
            getPostsResponseCallback.responseWSError("Error al consultar los posts");
        }
    }

    private void onError(Throwable throwable, GetPostsResponseCallback getPostsResponseCallback) {
        if(BuildConfig.DEBUG){
            throwable.printStackTrace();
        }
        if (throwable instanceof HttpException) {
            getPostsResponseCallback.onServerError();
        }else {
            getPostsResponseCallback.onNetworkConnectionError();
        }
    }
}
