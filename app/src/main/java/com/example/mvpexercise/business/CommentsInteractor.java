package com.example.mvpexercise.business;

import android.content.Context;

import com.example.mvpexercise.BuildConfig;
import com.example.mvpexercise.api.client.PostsRestService;
import com.example.mvpexercise.api.model.Comment;
import com.example.mvpexercise.api.model.GetCommentsResponseCallback;
import com.example.mvpexercise.application.Application;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import retrofit2.HttpException;

public class CommentsInteractor {
    private Application application;
    private PostsRestService postsRestService;
    private Context context;
    private Disposable disposable = Disposables.empty();

    public CommentsInteractor(Context context) {
        this.application = Application.getInstance(context);
        this.postsRestService = application.getPostsRestService();
        this.context = context;
    }

    public void getComments(String postId, GetCommentsResponseCallback callback){
        disposable = this.postsRestService.getComments(postId).subscribeOn(application.SubscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> onSuccess(comments, callback),
                        throwable -> onError(throwable, callback));
    }
    @SuppressWarnings("unchecked")
    private void onSuccess(ArrayList<Comment> comments, GetCommentsResponseCallback getCommentsResponseCallback){
        if(comments != null && comments.size() > 0){
            getCommentsResponseCallback.responseWSOk(comments);
        } else{
            getCommentsResponseCallback.responseWSError("Error al consultar los comments");
        }
    }

    private void onError(Throwable throwable, GetCommentsResponseCallback getCommentsResponseCallback) {
        if(BuildConfig.DEBUG){
            throwable.printStackTrace();
        }
        if (throwable instanceof HttpException) {
            getCommentsResponseCallback.onServerError();
        }else {
            getCommentsResponseCallback.onNetworkConnectionError();
        }
    }
}
