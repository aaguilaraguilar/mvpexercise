package com.example.mvpexercise.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mvpexercise.BR;
import com.example.mvpexercise.R;
import com.example.mvpexercise.api.model.Post;
import com.example.mvpexercise.databinding.ItemPostBinding;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> implements OnClickPostListener {

    private List<Post> postList;
    private Context context;
    private OnClickPostListener listener;

    public PostsAdapter(List<Post> postList, Context ctx, OnClickPostListener listener) {
        this.postList = postList;
        context = ctx;
        this.listener = listener;
    }

    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        ItemPostBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_post, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = postList.get(position);
        holder.bind(post);
        holder.binding.setItemClickListener(this);
    }


    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public void postClicked(Post post) {
        listener.postClicked(post);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemPostBinding binding;

        public ViewHolder(ItemPostBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Post item) {
            binding.setVariable(BR.model, item);
            binding.executePendingBindings();
        }
    }
}
