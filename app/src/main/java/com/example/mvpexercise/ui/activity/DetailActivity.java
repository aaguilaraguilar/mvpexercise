package com.example.mvpexercise.ui.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.example.mvpexercise.R;
import com.example.mvpexercise.api.model.Comment;
import com.example.mvpexercise.databinding.ActivityDetailBinding;
import com.example.mvpexercise.presenter.CommentsPresenter;
import com.example.mvpexercise.ui.adapter.CommentsAdapter;
import com.example.mvpexercise.utils.Constants;
import com.example.mvpexercise.view.CommentsView;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements CommentsView {

    private ActivityDetailBinding binding;
    private CommentsPresenter presenter;
    private CommentsAdapter commentsAdapter;
    private int postId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        presenter = new CommentsPresenter();
        presenter.setView(this);
        getPostComments();
    }

    private void getPostComments() {
        postId = getIntent().getIntExtra(Constants.INTENT_EXTRA_POST_ID, 0);
        presenter.getComments(Integer.toString(postId));
    }

    @Override
    public void loadComments(ArrayList<Comment> comments) {
        commentsAdapter = new CommentsAdapter(comments, this);
        binding.commentsList.setAdapter(commentsAdapter);
        binding.commentsList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void hideLoading() {
        binding.progressBarView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        binding.progressBarView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error,Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

}
