package com.example.mvpexercise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.example.mvpexercise.R;
import com.example.mvpexercise.api.model.Post;
import com.example.mvpexercise.databinding.ActivityMainBinding;
import com.example.mvpexercise.presenter.PostsPresenter;
import com.example.mvpexercise.ui.adapter.OnClickPostListener;
import com.example.mvpexercise.ui.adapter.PostsAdapter;
import com.example.mvpexercise.utils.Constants;
import com.example.mvpexercise.view.PostsView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PostsView, OnClickPostListener {

    // Store the binding
    private ActivityMainBinding binding;
    private PostsPresenter postsPresenter;
    private PostsAdapter postsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        postsPresenter = new PostsPresenter();
        postsPresenter.setView(this);

    }

    @Override
    public void loadPosts(ArrayList<Post> posts) {
        postsAdapter = new PostsAdapter(posts, this, this);
        binding.postsList.setAdapter(postsAdapter);
        binding.postsList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void hideLoading() {
        binding.progressBarView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        binding.progressBarView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error,Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void postClicked(Post post) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_POST_ID, post.getId());
        startActivity(intent);
    }
}
