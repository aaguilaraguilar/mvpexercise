package com.example.mvpexercise.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mvpexercise.BR;
import com.example.mvpexercise.R;
import com.example.mvpexercise.api.model.Comment;
import com.example.mvpexercise.databinding.ItemCommentBinding;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private List<Comment> commentList;
    private Context context;

    public CommentsAdapter(List<Comment> commentList, Context ctx) {
        this.commentList = commentList;
        context = ctx;
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        ItemCommentBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_comment, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.bind(comment);
    }


    @Override
    public int getItemCount() {
        return commentList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemCommentBinding binding;

        public ViewHolder(ItemCommentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Comment item) {
            binding.setVariable(BR.model, item);
            binding.executePendingBindings();
        }
    }
}
