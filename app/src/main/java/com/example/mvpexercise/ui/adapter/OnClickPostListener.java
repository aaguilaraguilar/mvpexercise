package com.example.mvpexercise.ui.adapter;

import com.example.mvpexercise.api.model.Post;

public interface OnClickPostListener {
    void postClicked(Post post);
}
