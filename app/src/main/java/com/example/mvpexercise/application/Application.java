package com.example.mvpexercise.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.mvpexercise.api.client.FactoryPostsRestService;
import com.example.mvpexercise.api.client.PostsRestService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class Application extends MultiDexApplication {
    private PostsRestService postsRestService;
    private Scheduler mScheduler;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static Application getInstance(Context context) {
        return (Application) context.getApplicationContext();
    }

    public PostsRestService getPostsRestService() {
        if (postsRestService == null) postsRestService = FactoryPostsRestService.createRestService();

        return postsRestService;
    }

    public Scheduler SubscribeScheduler() {
        if (mScheduler == null) mScheduler = Schedulers.io();

        return mScheduler;
    }

    //User to change scheduler from tests
    public void setScheduler(Scheduler scheduler) {
        this.mScheduler = scheduler;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(Application.this);
    }
}
