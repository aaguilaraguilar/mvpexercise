package com.example.mvpexercise.api.model;
import java.util.ArrayList;

public interface GetPostsResponseCallback {
    void responseWSOk(ArrayList <Post> response);
    void responseWSError(String error);
    void onNetworkConnectionError();
    void onServerError();
}
