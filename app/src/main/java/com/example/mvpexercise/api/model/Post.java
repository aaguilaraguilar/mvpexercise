package com.example.mvpexercise.api.model;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("body")
    private String body;

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private int id;

    @SerializedName("userId")
    private int userid;

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public int getUserid() {
        return userid;
    }


    @Override
    public String toString() {
        return "Post{" +
                "body='" + body + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                ", userid=" + userid +
                '}';
    }
}
