package com.example.mvpexercise.api.client;

import com.example.mvpexercise.api.model.Comment;
import com.example.mvpexercise.api.model.Post;
import com.example.mvpexercise.utils.Constants;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PostsRestService {
    @GET(Constants.POSTS_ENDPOINT)
    Observable<ArrayList<Post>> getPosts();

    @GET(Constants.GET_COMMENTS_ENDPOINT)
    Observable<ArrayList<Comment>> getComments(@Query("postId") String postId);
}