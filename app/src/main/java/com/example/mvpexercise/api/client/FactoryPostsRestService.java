package com.example.mvpexercise.api.client;

import com.example.mvpexercise.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FactoryPostsRestService {

    public static PostsRestService createRestService(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.POSTS_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(PostsRestService.class);
    }

}
