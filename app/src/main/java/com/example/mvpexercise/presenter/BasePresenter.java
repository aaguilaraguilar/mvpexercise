package com.example.mvpexercise.presenter;


public interface BasePresenter<V> {
    void setView(V view);
    void detachView();
}
