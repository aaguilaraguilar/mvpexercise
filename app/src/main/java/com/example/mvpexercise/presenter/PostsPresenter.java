package com.example.mvpexercise.presenter;

import com.example.mvpexercise.api.model.GetPostsResponseCallback;
import com.example.mvpexercise.api.model.Post;
import com.example.mvpexercise.business.PostsInteractor;
import com.example.mvpexercise.view.PostsView;

import java.util.ArrayList;

public class PostsPresenter implements BasePresenter<PostsView>, GetPostsResponseCallback {

    private PostsView postsView;
    private PostsInteractor postsInteractor;

    @Override
    public void setView(PostsView view) {
        if (view == null) throw new IllegalArgumentException("You can't set a null view");
        postsView = view;
        postsInteractor = new PostsInteractor(postsView.getContext());
        postsView.showLoading();
        postsInteractor.getPosts(this);
    }

    @Override
    public void detachView() {
        postsView = null;
    }

    @Override
    public void responseWSOk(ArrayList<Post> response) {
        postsView.hideLoading();
        postsView.loadPosts(response);
    }

    @Override
    public void responseWSError(String error) {
        postsView.hideLoading();
        postsView.showError(error);
    }

    @Override
    public void onNetworkConnectionError() {
        postsView.hideLoading();
        postsView.showError("Error de conexión, por favor verifique que se encuentra conectado a Internet");
    }

    @Override
    public void onServerError() {
        postsView.hideLoading();
        postsView.showError("Error del servidor, por favor intente más tarde.");
    }
}
