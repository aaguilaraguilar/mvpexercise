package com.example.mvpexercise.presenter;

import com.example.mvpexercise.api.model.Comment;
import com.example.mvpexercise.api.model.GetCommentsResponseCallback;
import com.example.mvpexercise.business.CommentsInteractor;
import com.example.mvpexercise.view.CommentsView;

import java.util.ArrayList;

public class CommentsPresenter implements BasePresenter<CommentsView>, GetCommentsResponseCallback {

    private CommentsView commentsView;
    private CommentsInteractor commentsInteractor;

    @Override
    public void responseWSOk(ArrayList<Comment> response) {
        commentsView.hideLoading();
        commentsView.loadComments(response);
    }

    @Override
    public void responseWSError(String error) {
        commentsView.hideLoading();
        commentsView.showError(error);
    }

    @Override
    public void onNetworkConnectionError() {
        commentsView.hideLoading();
        commentsView.showError("Error de conexión, por favor verifique que se encuentra conectado a Internet");
    }

    @Override
    public void onServerError() {
        commentsView.hideLoading();
        commentsView.showError("Error del servidor, por favor intente más tarde.");
    }

    @Override
    public void setView(CommentsView view) {
        if (view == null) throw new IllegalArgumentException("You can't set a null view");
        commentsView = view;
        commentsInteractor = new CommentsInteractor(commentsView.getContext());
    }

    public void getComments(String postID){
        commentsView.showLoading();
        commentsInteractor.getComments(postID,this);
    }

    @Override
    public void detachView() {
        commentsView = null;
    }
}
